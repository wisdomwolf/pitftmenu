#!/bin/bash

if [ "$EUID" != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

export SDL_FBDEV=/dev/fb1
export SDL_VIDEODRIVER=fbcon
export SDL_MOUSEDEV=/dev/input/touchscreen
export SDL_MOUSEDRV=TSLIB
python3 /home/pi/pitftmenu/pi_menu_qvga.py
